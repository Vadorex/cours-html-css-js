var infosLiens = () => {
    var longueur =document.getElementsByTagName("a").length;
    console.log(longueur);
    
    console.log(document.querySelector("a").href);
    var listeElt = document.getElementsByTagName("a");
    console.log(listeElt[longueur-1].getAttribute("href"));
}

var possede = (id, classe) => {
    var listeInstr = document.querySelectorAll("ul > li");
    var instrument;
    
    instrument = document.getElementById(id);
    if(instrument !== null){
        console.log(instrument.classList.contains(classe));
    }else
        console.log("Aucun élément ne possède l'id "+id);
}

infosLiens();

possede("saxophone", "bois"); // Doit afficher true
possede("saxophone", "cuivre"); // Doit afficher false
possede("trompette", "cuivre"); // Doit afficher true
possede("contrebasse", "cordes"); // Doit afficher une erreur