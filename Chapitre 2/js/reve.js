// TODO : écrire la fonction compterElements
var compterElements = (Elt) => {
    var nombreElt;
    nombreElt = document.querySelectorAll(Elt);
    return nombreElt;
}

console.log(compterElements("p")); // Doit afficher 4
console.log(compterElements(".adjectif")); // Doit afficher 3
console.log(compterElements("p .adjectif")); // Doit afficher 3
console.log(compterElements("p > .adjectif")); // Doit afficher 2