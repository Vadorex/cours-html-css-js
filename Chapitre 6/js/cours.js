var pseudoElt = document.getElementById("pseudo");
pseudoElt.value = "monPseudo";

pseudoElt.addEventListener("focus", function() {
   document.getElementById("aidePseudo").textContent = "Entrer votre pseudo" ;
});

// Suppression du message contextuel pour la saisie du pseudo
pseudoElt.addEventListener("blur", function (e) {
    document.getElementById("aidePseudo").textContent = "";
});

document.getElementById("confirmation").addEventListener("change",function(e){
   console.log("Demande de confirmation " + e.target.checked); 
});

document.getElementById("mdp").addEventListener("input", function(e){
    var mdp = e.target.value;
    var longueurMdp = "faible";
    var couleurMsg = "red";
    if(mdp.length >= 8){
        longueurMdp = "suffisante";
        couleurMsg = "green";
    }else if (mdp.length <= 4) {
        longueurMdp = "moyenne";
        couleurMsg = "orange";
    }
    var aideMdpElt = document.getElementById("aideMdp");
    aideMdpElt.textContent = "Longueur :  " + longueurMdp;
    aideMdpElt.style.color = couleurMsg; 
});

document.getElementById("courriel").addEventListener("blur", function(e){
    var validiteCourriel = "";
    if(e.target.value.indexOf("@") === -1)
        validiteCourriel = "Adresse invalide";
    document.getElementById("aideCourriel").textContent = validiteCourriel;
});