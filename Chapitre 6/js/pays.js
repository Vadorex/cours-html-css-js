// Liste des pays
var listePays = [
    "Afghanistan",
    "Afrique du Sud",
    "Albanie",
    "Algérie",
    "Allemagne",
    "Andorre",
    "Angola",
    "Anguilla",
    "Antarctique",
    "Antigua-et-Barbuda",
    "Antilles néerlandaises",
    "Arabie saoudite",
    "Argentine",
    "Arménie",
    "Aruba",
    "Australie",
    "Autriche",
    "Azerbaïdjan"
];

var firstLetterUp = (word) => {
    var firstLetter = word.charAt(0).toUpperCase();
    var modifiedW = "";
    var endOfWord = word.substr(1).toLowerCase();
    
    modifiedW = firstLetter + endOfWord;
    return modifiedW;
}

document.getElementById("pays").addEventListener("input", function(e){
    var saisie = e.target.value;
    
    saisie = firstLetterUp(saisie);
    console.log(saisie);
    // Clear
    document.getElementById("suggestions").innerHTML = "";
    
    for(var i = 0; i < listePays.length; i++){
        var suggestionW = document.createElement("div");
        var indexCorres = listePays[i].indexOf(saisie);
        
        suggestionW.classList.add("suggestion");
        
        
        if(indexCorres !== -1){
            suggestionW.textContent = listePays[i];
            document.getElementById("suggestions").append(suggestionW);
            
        }
    
    }
     document.getElementById("suggestions").addEventListener("click", function(e){
        document.getElementById("pays").value = e.target.textContent;
    });
});

document.getElementById("pays").addEventListener("blur", function(e){
     // Clear
    document.getElementById("suggestions").innerHTML = "";
});
