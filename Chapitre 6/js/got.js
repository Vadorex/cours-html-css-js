// Liste de quelques maisons de Game of Thrones. Chaque maison a un code et un nom
var maisons = [
    {
        code: "ST",
        nom: "Stark"
    },
    {
        code: "LA",
        nom: "Lannister"
    },
    {
        code: "BA",
        nom: "Baratheon"
    },
    {
        code: "TA",
        nom: "Targaryen"
    }
];

// Renvoie un tableau contenant quelques personnages d'une maison
function getPersonnages(codeMaison) {
    switch (codeMaison) {
    case "ST":
        return ["Eddard", "Catelyn", "Robb", "Sansa", "Arya", "Jon Snow"];
    case "LA":
        return ["Tywin", "Cersei", "Jaime", "Tyrion"];
    case "BA":
        return ["Robert", "Stannis", "Renly"];
    case "TA":
        return ["Aerys", "Daenerys", "Viserys"];
    default:
        return [];
    }
}
// add houses in the DOM
window.addEventListener("load", function(e){
    var listeMaisons = document.getElementById("maison");
    
    for(var i = 0; i < maisons.length; i++){
        var addMaison = document.createElement("option");
        addMaison.value = maisons[i].code;
        addMaison.textContent = maisons[i].nom;
        listeMaisons.append(addMaison);
    }
});

document.getElementById("maison").addEventListener("change", function(e){
    var maisonSelected = e.target.value;
    let tabPerso = getPersonnages(maisonSelected);
    
    // Clear
    document.getElementById("persos").innerHTML = "";
    
    for(let i = 0; i < tabPerso.length; i++){
        var listeElt = document.createElement("li");
        listeElt.textContent = tabPerso[i];
        document.getElementById("persos").append(listeElt);
    }
});












