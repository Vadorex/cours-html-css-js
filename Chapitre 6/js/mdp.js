document.getElementById("mdp1").addEventListener("blur", function(e){
    if(e.target.value.length < 6){
        document.getElementById("infoMdp").textContent = "Erreur : la longueuer minimale du mot de passe est de 6 caractères"
    }
});

document.getElementById("mdp2").addEventListener("blur", function(e){
    if(e.target.value !== document.getElementById("mdp1").value){
        document.getElementById("infoMdp").textContent = "Erreur : les deux mots de passe doivent être identiques"
    }
});

var form = document.querySelector("form");

form.addEventListener("submit", function(e){
    var regex = /\d/;
    var mdp1 = document.getElementById("mdp1");
    var mdp2 = document.getElementById("mdp2"); 
    
    if(!regex.test(document.getElementById("mdp1").value)){
        document.getElementById("infoMdp").textContent = "Erreur : le mot de passe doit contenir au moins 1 chiffre";
        
        if(mdp1.value.length < 6){
            var sautLigne = document.createElement("br");
            document.getElementById("infoMdp").append(sautLigne);
            document.getElementById("infoMdp").append("Erreur : la longueuer minimale du mot de passe est de 6 caractères"); 
        }
        if(mdp2.value !== mdp1.value){
            var sautLigne = document.createElement("br");
            document.getElementById("infoMdp").append(sautLigne);
            document.getElementById("infoMdp").append("Erreur : les deux mots de passe doivent être identiques");
        }
        console.log(document.nodeName);
        e.preventDefault();
    } else
        document.getElementById("infoMdp").textContent = "Envoyer !";
   
});

