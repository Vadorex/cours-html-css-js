// Liste des mots du dictionnaire
var mots = [
    {
        terme: "Procrastination",
        definition: "Tendance pathologique à remettre systématiquement au lendemain"
    },
    {
        terme: "Tautologie",
        definition: "Phrase dont la formulation ne peut être que vraie"
    },
    {
        terme: "Oxymore",
        definition: "Figure de style qui réunit dans un même syntagme deux termes sémantiquement opposés"
    }
];

// TODO : créer le dictionnaire sur la page web, dans la div "contenu"
//"d1" "dt" "dd"
var debutBalise = document.createElement("d1");
document.getElementById("contenu").append(debutBalise);

for(let i = 0; i < mots.length; i++){
    var newMot = document.createElement("dt");
    var def = document.createElement("dd");
    var mev = document.createElement("strong");
    
    mev.textContent = mots[i].terme;
    
    def.textContent = mots[i].definition;
    
    document.querySelector("d1").append(newMot);
    document.querySelector("d1").append(def);
    newMot.append(mev);
}