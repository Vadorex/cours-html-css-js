//ajaxGet("https://api.github.com/users/");
let formElt = document.querySelector("form");

formElt.addEventListener("submit",function(e){
    e.preventDefault();
    
    var entreeR = document.getElementById("recherche");
    entreeR = entreeR.value;
    ajaxGet("https://api.github.com/users/"+entreeR, function(reponse){
        let info = JSON.parse(reponse);
        let avatar = info.avatar_url;
        let name = info.name;
        let website = info.blog;
        
        let imageB = document.createElement("img");
        let nomB = document.createElement("div");
        let websiteB = document.createElement("a");
        let employeurB = document.createElement("div");
        
        imageB.src = avatar;
        imageB.style.height = "150px";
        imageB.style.width = "150px";
        nomB.textContent = name;
        websiteB.href = website;
        websiteB.textContent = website;
        
        document.getElementById("infos").innerHTML = "";
        document.getElementById("infos").append(imageB);
        document.getElementById("infos").append(nomB);
        document.getElementById("infos").append(employeurB);
        employeurB.append(websiteB);

    });
})
