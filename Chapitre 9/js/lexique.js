let lettres = document.getElementById("lettres");

lettres.addEventListener("click", function(e){
    let regex = /[A-Z]/;

    if(regex.test(e.target.textContent) && e.target.textContent.length < 2){
        ajaxGet("https://oc-jswebsrv.herokuapp.com/api/lexique/"+e.target.textContent, function(reponse){
                   
            document.getElementById("definitions").innerHTML = "";
            let data = JSON.parse(reponse);
            data.forEach((mot) => {
                let h3 = document.createElement("h3");
                let p = document.createElement("p");
                h3.textContent = mot.term;
                p.textContent = mot.definition;
         
                document.getElementById("definitions").append(h3);
                document.getElementById("definitions").append(p);
                
            })
        });
    }else{
        document.getElementById("definitions").innerHTML = "";
        document.getElementById("definitions").textContent = "No word found.";
    }
        

})