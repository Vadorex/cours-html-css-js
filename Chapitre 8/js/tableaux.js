ajaxGet("http://localhost:8888/javascript-web-srv/data/tableaux.json", function (reponse) {
    let tab = JSON.parse(reponse);
    tab.forEach((peinture) => {
        let tr = document.createElement("tr");
        let tdName = document.createElement("td");
        let tdYear = document.createElement("td");
        let tdAutor = document.createElement("td");
        tdName.textContent = peinture.titre;
        tdYear.textContent = peinture.annee;
        tdAutor.textContent = peinture.peintre;
        
        document.getElementById("tableau").append(tr);
        tr.append(tdName);
        tr.append(tdYear);
        tr.append(tdAutor);
    })
    
});
