// Liste des questions à afficher. Une question est définie par son énoncé et sa réponse
var questions = [
    {
        enonce: "Combien font 2+2 ?",
        reponse: "2+2 = 4"
    },
    {
        enonce: "En quelle année Christophe Colomb a-t-il découvert l'Amérique ?",
        reponse: "1492"
    },
    {
        enonce: "On me trouve 2 fois dans l'année, 1 fois dans la semaine, mais pas du tout dans le jour... Qui suis-je ?",
        reponse: "La lettre N"
    }
];


for( let i = 0; i < questions.length; i++){
    // Declaration
    var Eltliste = document.createElement("p");
    var questionG = document.createElement("strong");
    var button = document.createElement("button");
    
    // Modif contenu
    questionG.textContent = "Question " + (i+1) + " : ";
    Eltliste.textContent = questions[i].enonce;
    button.textContent = "Afficher la réponse";
    
    button.addEventListener("click", function(e){
       var reponseAff = document.createElement("i");
        reponseAff.textContent = questions[i].reponse;
        e.target.parentNode.replaceChild(reponseAff, e.target);
    });
    
    // Ajout des éléments
    document.getElementById("contenu").append(Eltliste);
    Eltliste.prepend(questionG);
    document.getElementById("contenu").append(button);
    
}

