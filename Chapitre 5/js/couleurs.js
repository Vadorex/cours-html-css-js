var tabDiv = document.getElementsByTagName("div");

var backgroundColor = (color) => {
    for(let i = 0; i < tabDiv.length; i++){
        tabDiv[i].style.background = color;
    }
}

document.addEventListener("keypress", function(e){
    let touchePress = String.fromCharCode(e.charCode);
    
    if(touchePress === "r" || touchePress === "R"){
       backgroundColor("red");
   }
    if(touchePress === "v" || touchePress === "V"){
       backgroundColor("green");
   }
    if(touchePress === "b" || touchePress === "B"){
       backgroundColor("blue");
   }
    if(touchePress === "j" || touchePress === "J"){
       backgroundColor("yellow");
   } 
});